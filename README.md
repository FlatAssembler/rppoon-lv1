# RPPOON LV1

Laboratory exercise from the course "Razvoj programske podrške objektno orijentiranim načelima" at the FERIT university. It was supposed to be done using Microsoft Visual Studio, but I failed to install it. Nevertheless, I managed to install Microsoft Visual C# Compiler for Linux, so I was able to do the exercise using it. Basic testing is applied to all tasks, even though it's only required for the last task.
