//Task 2: Create a class that represents a note with a known author which can't be changed.
using System;
class Note {
    private bool isAuthorSet;
    private string privateAuthor;
    public string author{
        get{return privateAuthor;}
        set{
            if (isAuthorSet) {
                throw new Exception("Author can't be set more than once!");
            }
            else {
                isAuthorSet=true;
                privateAuthor=value;
            }
        }
    }
    public int getPriority() {
        return priority;
    }
    public void setPriority(int newPriority) {
        priority=newPriority;
    }
    public string getAuthor() {
        return author;
    }
    public void setAuthor(string newAuthor) {
        author=newAuthor;
    }
    public void setText(string newText) {
        text=newText;
    }
    public string getText() {
        return text;
    }
    public int priority {get; set;}
    public string text {get; set;}
    public Note() {
        isAuthorSet=false;
    }
    public Note(String newAuthor, String newText, int newPriority) {
        priority=newPriority;
        author=newAuthor;
        text=newText;
        isAuthorSet=true;
    }
    public Note(String newText, int newPriority) {
        text=newText;
        priority=newPriority;
        isAuthorSet=false;
    }
    public Note(String newText) {
        text=newText;
        isAuthorSet=false;
    }
    public override string ToString() {
        return
            "Author: "+((isAuthorSet)?(author):("Anonymous"))+"\n"+
            "Priority: "+priority+"\n"+
            "Body: "+text;
    }
}
