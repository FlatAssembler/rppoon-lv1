//Task 6: A class representing a note that remembers when it was created.
using System;
class NoteWithTime : Note {
    public DateTime creationTime {get; set;}
    public NoteWithTime() :base() {
        creationTime=DateTime.Now;
    }
    public NoteWithTime(String newAuthor, String newText, int newPriority, DateTime newCreationTime)
            : base (newAuthor,newText,newPriority) {
        creationTime=newCreationTime;
    } 
    public NoteWithTime(String newAuthor, String newText, int newPriority)
            : base(newAuthor,newText,newPriority) {
        creationTime=DateTime.Now;
    }
    public NoteWithTime(String newText, int newPriority)
            : base(newText,newPriority) {
        creationTime=DateTime.Now;
    }
    public NoteWithTime(String newText)
            : base(newText) {
        creationTime=DateTime.Now;
    }
    public override string ToString() {
        return "Date Created: "+creationTime.ToString()+"\n"+base.ToString();
    }
}