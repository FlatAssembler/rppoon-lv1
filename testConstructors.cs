//Task 3: Testing the constructors.
using System;
class TestConstructors {
    public static void Main() {
        Note firstNote=new Note("John Doe","First note",1);
        Note secondNote=new Note("Second note",2);
        Note thirdNote=new Note("Third note");
        Console.WriteLine(firstNote.toString());
        Console.WriteLine(secondNote.toString());
        Console.WriteLine(thirdNote.toString());
    }
}