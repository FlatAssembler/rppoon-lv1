//Part of the Task 2, see "note.cs" for the main part.
using System;
class TestNote {
    public static void Main() {
        Note testedNote=new Note("Hello world!");
        testedNote.priority=1;
        testedNote.author="Teo Samarzija";
        Console.WriteLine(testedNote.ToString());
        try {
            testedNote.author="John Doe";
        }
        catch(Exception exception) {
            Console.WriteLine(exception.Message);
        }
    }
}