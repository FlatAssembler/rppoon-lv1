//A part of the task 6, see "noteWithTime.cs" for more information.
//Compile with: "csc note.cs testNoteWithTime.cs noteWithTime.cs"
using System;
class TestNoteWithTime {
    public static void Main() {
        NoteWithTime testedNote=new NoteWithTime("John Doe","Hello world!",1);
        Console.WriteLine(testedNote.ToString());
    }
}