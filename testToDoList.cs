//A part of the Task #7, see "toDoList.cs" for more.
//Compile with "csc testToDoList.cs toDoList.cs note.cs noteWithTime.cs"
using System;
class TestToDoList {
    public static void Main() {
        ToDoList testedList=new ToDoList();
        int n;
        Console.WriteLine("Enter your name and press ENTER:");
        string userName=Console.ReadLine();
        Console.WriteLine("Enter how many notes you will enter (at least 3) and then press ENTER:");
        n=Int32.Parse(Console.ReadLine());
        if (n<3) throw new IndexOutOfRangeException("You need to enter at least 3 notes!");
        for (int i=0; i<n; i++) {
            Console.WriteLine("Enter the priority of the note #"+(i+1)+" and press ENTER:");
            int priority=Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the body of the note (a string of characters such as spaces, letters and digits) and press ENTER:");
            string body=Console.ReadLine();
            testedList.addNote(new NoteWithTime(userName,body,priority));
        }
        Console.WriteLine("The to-do-list now looks like this:");
        Console.WriteLine(testedList.ToString());
        Console.WriteLine("Deleting the notes with the highest priority...");
        testedList.deleteTheHighestPriorityNotes();
        Console.WriteLine("After you finish the most important tasks, the list will look like this:");
        Console.WriteLine(testedList.ToString());
    }
}