//Task #7: Make a class that represents a to-do list made of the notes.
using System;
using System.Collections.Generic;
class ToDoList {
    private List<Note> listOfTasks;
    public void addNote(Note newNote) {
        listOfTasks.Add(newNote);
    }
    public List<Note> notesWithHighestPriority() {
        List<Note> listToReturn=new List<Note>();
        if (listOfTasks.Count==0) return listToReturn;
        for (int i=0; i<listOfTasks.Count; i++)
            if (listOfTasks[i].priority==highestPriority())
                listToReturn.Add(listOfTasks[i]);
        return listToReturn;
    }
    public int highestPriority() {
        if (listOfTasks.Count==0) throw new IndexOutOfRangeException("The list is empty!");
        int highestPriority=listOfTasks[0].priority;
        for (int i=0; i<listOfTasks.Count; i++)
            if (listOfTasks[i].priority>highestPriority)
                highestPriority=listOfTasks[i].priority;
        return highestPriority;
    }
    public void deleteTheHighestPriorityNotes() {
        int currentHighestPriority=this.highestPriority();
        for (int i=0; i<listOfTasks.Count; i++)
            if (listOfTasks[i].priority==currentHighestPriority) {
                listOfTasks.RemoveAt(i);
                i--; //Without this, two consecutive notes with the highest priority will result in only one of them being deleted.
            }
    }
    public override string ToString() {
        string stringToReturn="";
        const int numberOfDashesToSeparateNotes=32;
        for (int i=0; i<numberOfDashesToSeparateNotes; i++)
            stringToReturn+='*';
        stringToReturn+='\n';
        for (int i=0; i<listOfTasks.Count; i++) {
            stringToReturn+=listOfTasks[i].ToString()+"\n";
            for (int j=0; j<numberOfDashesToSeparateNotes; j++)
                stringToReturn+='-';
            stringToReturn+='\n';
        }
        return stringToReturn;
    }
    public Note this[int index] { //Overloading the operator [] to give access to particular notes.
        get {return listOfTasks[index];}
        set {listOfTasks[index]=value;}
    }
    public ToDoList(){
        listOfTasks=new List<Note>();
    }
}